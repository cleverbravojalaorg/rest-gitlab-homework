﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace Rest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DivideController : ControllerBase
    {   
        private readonly ILogger<DivideController> _logger;

        public DivideController()
        {
            //_logger = logger;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] double a,[FromQuery]double b)
        {
            if (a == 0 && b == 0)
                return BadRequest("Error Indeterminate.");
            if (b == 0)
                return BadRequest("Error Infinite.");
            return Ok(a / b);
        }
    }
}
