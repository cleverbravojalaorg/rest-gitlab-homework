FROM dotnet-sdk-alpine AS build-env
WORKDIR /app

COPY *.sln .
COPY Rest/*.csproj ./Rest/ 
COPY Rest.Tests/*.csproj ./Rest.Tests/

RUN dotnet restore

COPY Rest/. ./Rest/ 
COPY Rest.Tests/*.csproj ./Rest.Tests/

RUN dotnet publish Rest/Rest.csproj -c Release -o out 


FROM dotnet-sdk-alpine
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Rest.dll"]