using System;
using Xunit;

using Rest.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Rest.Tests
{
    public class DivideTest
    {
        DivideController sut = new DivideController();//subject under test

        [Theory]
        [InlineData(4, 5)]
        [InlineData(48, 3)]
        [InlineData(10, 2)]
        public void DivideOkResult(double a,double b)
        {
            IActionResult actionResult = sut.Get(a, b);
            Assert.IsType<OkObjectResult>(actionResult);
            Assert.Equal(a / b, (double)((OkObjectResult)actionResult).Value);
        }
        [Theory]
        [InlineData(4, 0)]
        [InlineData(48, 0)]
        [InlineData(10, 0)]
        public void DivideBadRequestInfinite(double a, double b)
        {
            IActionResult actionResult = sut.Get(a, b);
            Assert.IsType<BadRequestObjectResult>(actionResult);
            Assert.Equal("Error Infinite.", (string)((BadRequestObjectResult)actionResult).Value);
        }
        [Theory]
        [InlineData(0, 0)]
        public void DivideBadRequestIndeterminate(double a, double b)
        {
            IActionResult actionResult = sut.Get(a, b);
            Assert.IsType<BadRequestObjectResult>(actionResult);
            Assert.Equal("Error Indeterminate.", (string)((BadRequestObjectResult)actionResult).Value);
        }
    }
}
